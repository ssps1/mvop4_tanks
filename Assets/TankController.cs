using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class TankController : NetworkBehaviour
{
    public GameObject targetPrefab;
    public Rigidbody target;
    public Vector3 offset;


    public Transform[] raycastPoints;
    public float raycastDistance = 1.2f;
    public LayerMask mask;

    public float movementForce = 10;
    public float turnSpeed = 10;

    public Transform turretWeapon;
    public float turretMinimalAngle;
    public float turretMaximalAngle;


    [SyncVar(hook = nameof(OnTurretAngleChanged))]
    public float turretVerticalAim; //0 - 1 range to lerp from min to max angle

    private Scrollbar tempScrollBar;

    public float shootingCooldown = 1f;
    private float timeOfLastShot;

    public Transform projectileSpawnTransform;
    public GameObject projectilePrefab;
    public float projectileForce;
    public float projectileForceTankPushBack;

    public GameObject localCollider;


    void Start()
    {
        if (!isLocalPlayer) { return; }

        localCollider.SetActive(false);

        target = Instantiate(targetPrefab, transform.position, transform.rotation, null).GetComponent<Rigidbody>();

        tempScrollBar = GameObject.Find("Scrollbar").GetComponent<Scrollbar>();

    }


    void Update()
    {
        UpdateTurretAngle();

        if (!isLocalPlayer) { return; }


        forwardInput = Input.GetAxis("Vertical");
        sideInput = Input.GetAxis("Horizontal");

        DoRayCasts();

        transform.position = target.transform.position + offset;



        if(Input.GetKeyDown(KeyCode.Space))
            CmdShootRay();
    }

    
    void CmdShootRay()
    {
        Shoot();
    }

    /// <summary>
    /// Commands are sent from player objects on the client to player objects on the server. 
    /// </summary>
    [Command]
    void Shoot() {
        if (Time.time > timeOfLastShot + shootingCooldown) {
            GameObject go = Instantiate(projectilePrefab, projectileSpawnTransform.position, projectileSpawnTransform.rotation);
            go.GetComponent<Rigidbody>().AddForce(projectileSpawnTransform.forward * projectileForce);

            if (isLocalPlayer) { 
                target.AddForce(-projectileSpawnTransform.forward * projectileForceTankPushBack);
               
                timeOfLastShot = Time.time;
            }
            NetworkServer.Spawn(go);
        }
    }

    void UpdateTurretAngle ()
    {
        float angle = Mathf.Lerp(turretMinimalAngle, turretMaximalAngle, turretVerticalAim);

        turretWeapon.transform.localEulerAngles = new Vector3(angle, 0, 0);

    }

    float forwardInput;
    float sideInput;

    Vector3 relativeVelocity;

    private void FixedUpdate()
    {

        CmdSendTurretUpdate(tempScrollBar.value);
        if (!isLocalPlayer) { return; }
        Handling();

    }

    void OnTurretAngleChanged(float _Old, float _New)
    {
        turretVerticalAim = _New;
    }

    [Command]
    void CmdSendTurretUpdate(float value) {
        turretVerticalAim = value;
        
    }
    void Handling() {


        target.AddForce(transform.forward * movementForce * forwardInput);


        float sideFriction = 0.8f;

        relativeVelocity = transform.worldToLocalMatrix * target.velocity;
        relativeVelocity.x *= sideFriction;
        target.velocity = transform.localToWorldMatrix * relativeVelocity;

        transform.Rotate(transform.up,sideInput* turnSpeed);


    }

    void DoRayCasts() {
        List<Vector3> points = new List<Vector3>();

        foreach (var item in raycastPoints){
            RaycastHit rh = new RaycastHit();

            if (Physics.Raycast(item.position, Vector3.down, out rh, raycastDistance, mask)){
                points.Add(rh.point);
                Debug.DrawLine(item.position, rh.point, Color.red);
            }else{
                Debug.DrawRay(item.position, Vector3.down * raycastDistance, Color.green);
            }
        }

        if (points.Count < 4)
            return;

        Vector3 forwardDirection = (points[0] - points[1]).normalized;
        Vector3 cross = Vector3.Cross(forwardDirection,Vector3.up);
        Vector3 normal = Vector3.Cross(cross,forwardDirection);
        if (normal.y < 0) {
            normal.y *= -1;
        }

        Vector3 sideDirection = (points[2] - points[3]).normalized;
        Vector3 cross2 = Vector3.Cross(sideDirection, Vector3.up);
        Vector3 normal2 = Vector3.Cross(cross2, sideDirection);
        if (normal2.y < 0) {
            normal2.y *= -1;
        }

        Vector3 combinedNormal = Vector3.Lerp(normal,normal2,0.5f);


        transform.rotation = Quaternion.Slerp(transform.rotation ,
             Quaternion.FromToRotation(transform.up, combinedNormal)* transform.rotation, 0.1f);


    }
}

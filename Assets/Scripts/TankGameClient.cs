using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


public class TankGameClient : NetworkBehaviour
{

    [SyncVar(hook = nameof(OnTurretAngleChanged))]
    public float turretAngleX;

    private TankController tankController;

    void OnTurretAngleChanged(float _Old, float _New)
    {
        tankController.turretVerticalAim = _New;
    }


    void Start()
    {
        if (!isClient)
            return;
    }


    void Update()
    {
        if (!isClient)
            return;
    }

    private void FixedUpdate()
    {
        if (!isClient)
            return;

    }

    [Command]
    void UpdatePlayer() {
        PlayerData data = new PlayerData();
        data.connectionID = NetworkClient.connection.connectionId;
        data.pos = transform.position;
        data.rot = transform.rotation;
        data.scale = transform.localScale;
        data.turretEulerAngles = transform.GetComponent<TankController>().turretWeapon.localEulerAngles;
        
    }
}

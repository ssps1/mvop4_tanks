using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class Projectile : NetworkBehaviour
{
    public GameObject[] explosionPrefabs;

    
    
    // Start is called before the first frame update
    void Start()
    {

        Destroy(this.gameObject, 5);

    }

    // Update is called once per frame
    void Update()
    {

    }


    [ClientRpc]
    void SpawnExplosion(Vector3 pos) {

        GameObject go = Instantiate(explosionPrefabs[Random.Range(0, explosionPrefabs.Length)],
            pos, Quaternion.identity);
        Destroy(go, 3);
        Destroy(this.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {

        SpawnExplosion(transform.position);

            float radius = 3f;
            float power = 300f;
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(power, transform.position, radius, 3.0F);
            }
        


    }
}

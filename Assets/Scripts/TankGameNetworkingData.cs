using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public struct PlayerData : NetworkMessage
{
    public int connectionID;
    public Vector3 pos;
    public Quaternion rot;
    public Vector3 scale;
    public Vector3 turretEulerAngles;


}